import 'react-app-polyfill/ie9';
import React from 'react';
import ReactDOM from 'react-dom';
import './styles.css';
import { SearchPage } from './pages/SearchPage';
import { CreateSearchPageService } from './pages/SearchPage/SearchPage.service';
import { getInstance } from './common/axios-instance';

const axiosInstance = getInstance();
const searchPageApi = CreateSearchPageService(axiosInstance);

ReactDOM.render(
  <SearchPage service={searchPageApi} />,
  document.getElementById('app')
);
