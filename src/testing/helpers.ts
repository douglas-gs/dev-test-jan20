import { fireEvent } from '@testing-library/react';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const changeFieldValue = (
  input: HTMLElement,
  value: number | string
) => {
  fireEvent.change(input, { target: { value } });
};
