import {
  CreateSearchPageService,
  GetPickupLocationsResponse
} from './SearchPage.service';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { generateMockGetPickupLocationsResponse } from './SearchPage.service.mock';
import faker from 'faker';

faker.seed(1);

const axiosMock = new MockAdapter(axios);

beforeEach(() => {
  axiosMock.reset();
});

describe('Search Page service', () => {
  it('will return pickup locations', async () => {
    const mockGetPickupLocationsResponse: GetPickupLocationsResponse = generateMockGetPickupLocationsResponse();
    const searchTerm: string = faker.random.alphaNumeric(3);
    axiosMock
      .onGet(
        `FTSAutocomplete.do?solrIndex=fts_en&solrRows=6&solrTerm=${searchTerm}`
      )
      .reply(200, mockGetPickupLocationsResponse);

    const { getPickupLocations } = CreateSearchPageService(axios);
    const result = await getPickupLocations(searchTerm);

    expect(result.data).toEqual(mockGetPickupLocationsResponse);
  });
});
