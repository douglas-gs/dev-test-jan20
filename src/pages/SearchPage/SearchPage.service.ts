import { AxiosInstance, AxiosPromise } from 'axios';
import { numResultsToReturn } from '../../common/config';

export interface PickupLocation {
  bookingId: string;
  country: string;
  name: string;
  region: string;
  placeType: string;
  city?: string;
}

export interface GetPickupLocationsResponse {
  results: {
    docs: PickupLocation[];
  };
}

export interface SearchPageApi {
  getPickupLocations: (
    searchTerm: string
  ) => AxiosPromise<GetPickupLocationsResponse>;
}

export const CreateSearchPageService = (
  axios: AxiosInstance
): SearchPageApi => ({
  getPickupLocations: (
    searchTerm: string
  ): AxiosPromise<GetPickupLocationsResponse> =>
    axios.get(
      `FTSAutocomplete.do?solrIndex=fts_en&solrRows=${numResultsToReturn}&solrTerm=${searchTerm}`
    )
});
