import React from 'react';
import { render } from '@testing-library/react';
import { SearchPage } from './SearchPage';
import { getMockSearchPageService } from './SearchPage.service.mock';

const mockHelper = getMockSearchPageService();

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const renderComponent = () => {
  const utils = render(<SearchPage service={mockHelper.service} />);

  return {
    searchPage: utils.getByTestId('SearchPage'),
    searchWidget: utils.getByTestId('SearchWidget'),
    ...utils
  };
};

describe('Search page', () => {
  it('renders the component', () => {
    const { searchPage } = renderComponent();
    expect(searchPage).toBeInTheDocument();
  });
  it('renders the search widget', () => {
    const { searchWidget } = renderComponent();
    expect(searchWidget).toBeInTheDocument();
  });
});
