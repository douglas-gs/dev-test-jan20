import React, { FC } from 'react';
import './SearchPage.style.css';
import { SearchWidget } from './components/SearchWidget';
import { SearchPageApi } from './SearchPage.service';

interface Props {
  service: SearchPageApi;
}

export const SearchPage: FC<Props> = ({ service }: Props) => {
  return (
    <div data-testid="SearchPage" className="searchpage">
      <SearchWidget service={service} />
    </div>
  );
};
