import {
  PickupLocation,
  GetPickupLocationsResponse
} from './SearchPage.service';
import faker from 'faker';

faker.seed(1);

export const generateMockPickupLocations = (
  numPickupLocations = 6
): Array<PickupLocation> =>
  new Array(numPickupLocations).fill({}).map(() => ({
    bookingId: faker.random.alphaNumeric(10),
    country: faker.random.word(),
    name: faker.random.word(),
    region: faker.random.word(),
    placeType: faker.random.alphaNumeric(1)
  }));

export const generateMockGetPickupLocationsResponse = (): GetPickupLocationsResponse => ({
  results: {
    docs: generateMockPickupLocations()
  }
});

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const generateMockNoResultsPickupLocations = () => [
  {
    name: 'No results found'
  }
];

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const getMockSearchPageService = () => {
  const mockGetPickupLocationsResponse = generateMockGetPickupLocationsResponse();

  const service = {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    getPickupLocations: jest.fn((searchTerm: string) =>
      Promise.resolve({ data: mockGetPickupLocationsResponse })
    )
  };

  return {
    service,
    mockPickupLocations: mockGetPickupLocationsResponse.results.docs,
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    clearMocks: () => {
      service.getPickupLocations.mockClear();
    }
  };
};
