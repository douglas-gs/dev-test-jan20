import React from 'react';
import { render } from '@testing-library/react';
import { PickupLocationResults } from './PickupLocationResults';
import {
  generateMockPickupLocations,
  generateMockNoResultsPickupLocations
} from '../../../../SearchPage.service.mock';
import faker from 'faker';
import { PickupLocation } from '../../../../SearchPage.service';

const numMockPickupLocations = faker.random.number({ min: 5, max: 10 });
const mockPickupLocationResults = generateMockPickupLocations(
  numMockPickupLocations
);

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const renderComponent = () => {
  const utils = render(
    <PickupLocationResults searchResults={mockPickupLocationResults} />
  );

  return {
    pickupLocationResults: utils.getByTestId('pickupLocationResults'),
    pickupLocationResultItems: utils.getAllByTestId(
      'pickupLocationResultsItem'
    ),
    ...utils
  };
};

describe('Pickup Location search results', () => {
  it('renders', () => {
    const { pickupLocationResults } = renderComponent();
    expect(pickupLocationResults).toBeInTheDocument();
  });
  it('show the correct number of search results', () => {
    const { pickupLocationResultItems } = renderComponent();
    expect(pickupLocationResultItems.length).toBe(numMockPickupLocations);
  });
  it('show the correct information in each search result', () => {
    const { getByText } = renderComponent();
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const getByPickupLocation = (pickupLocation: PickupLocation) => {
      getByText(pickupLocation.name);
      getByText(`${pickupLocation.region}|${pickupLocation.country}`);
    };

    mockPickupLocationResults.forEach(getByPickupLocation);
  });
  it('show no results found message when no the search yields no results', () => {
    const mockNoResultsPickupLocationResults = generateMockNoResultsPickupLocations();
    const utils = render(
      <PickupLocationResults
        searchResults={mockNoResultsPickupLocationResults}
      />
    );

    utils.getByText('No results found');
  });
});
