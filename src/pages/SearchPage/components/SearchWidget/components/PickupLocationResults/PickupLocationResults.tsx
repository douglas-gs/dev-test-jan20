import React, { FC } from 'react';
import { PickupLocation } from '../../../../SearchPage.service';

interface Props {
  searchResults: PickupLocation[];
}

export const PickupLocationResults: FC<Props> = ({ searchResults }: Props) => (
  <div className="pickupLocation-results">
    <ol
      id="pickupLocationResults"
      className="pickupLocation-results__list"
      data-testid="pickupLocationResults"
    >
      {searchResults.map((searchResult, index) => (
        <li
          key={index}
          id={`pickupLocationResultsItem-${index}`}
          data-testid={`pickupLocationResultsItem`}
        >
          <div>
            <div>{searchResult.placeType}</div>
          </div>
          <div>
            <div>
              <span>
                <span>{searchResult.name}</span>
              </span>
            </div>
            <div>
              {searchResult.region}|{searchResult.country}
            </div>
          </div>
        </li>
      ))}
    </ol>
  </div>
);
