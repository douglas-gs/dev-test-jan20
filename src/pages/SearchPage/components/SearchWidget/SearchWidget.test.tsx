import React from 'react';
import { render, wait } from '@testing-library/react';
import { SearchWidget } from './SearchWidget';
import { getMockSearchPageService } from '../../SearchPage.service.mock';
import faker from 'faker';
import { changeFieldValue } from '../../../../testing/helpers';

const mockHelper = getMockSearchPageService();

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const renderComponent = () => {
  const utils = render(<SearchWidget service={mockHelper.service} />);

  return {
    searchWidget: utils.getByTestId('SearchWidget'),
    pickupLocation: utils.getByTestId('pickupLocation'),
    searchResultsList: utils.getByTestId('pickupLocationResults'),
    pickupLocationScreenReaderInstructions: utils.getByTestId(
      'pickupLocation-screenReaderInstructions'
    ),
    ...utils
  };
};

describe('Search widget', () => {
  it('renders', () => {
    const { searchWidget } = renderComponent();
    expect(searchWidget).toBeInTheDocument();
  });
  it('displays an appropriate heading', () => {
    const { getByText } = renderComponent();
    getByText('Let’s find your ideal car');
  });
  describe('has a textbox', () => {
    it('with an apprpriate label', () => {
      const { getByLabelText } = renderComponent();
      getByLabelText('Pick-up Location');
    });
    it('with an apprpriate placeholder', () => {
      const { pickupLocation } = renderComponent();
      expect(pickupLocation.getAttribute('placeholder')).toEqual(
        'city, airport, station, region, district…'
      );
    });
    it('that is described by the approriate page element', () => {
      const { pickupLocation } = renderComponent();
      expect(pickupLocation.getAttribute('aria-describedby')).toEqual(
        'pickupLocation-screenReaderInstructions'
      );
    });
  });
  it('has hidden screen reader information ', () => {
    const { pickupLocationScreenReaderInstructions } = renderComponent();
    expect(pickupLocationScreenReaderInstructions).toHaveClass('hidden');
    expect(pickupLocationScreenReaderInstructions).toHaveTextContent(
      'Use this input to search for pickup locations.'
    );
  });
  it('shows no results when a single character is entered', async () => {
    const { pickupLocation, getByTestId } = renderComponent();
    const singleCharacterSearchTerm = faker.random.alphaNumeric(1);
    changeFieldValue(pickupLocation, singleCharacterSearchTerm);

    await wait(() => {
      const pickupLocationResults = getByTestId('pickupLocationResults');
      expect(pickupLocationResults.children.length).toBe(0);
    });
  });
  it('shows results when mulitple characters are entered', async () => {
    const { pickupLocation, getAllByTestId } = renderComponent();
    const multipleCharacterSearchTerm = faker.random.alphaNumeric(2);
    changeFieldValue(pickupLocation, multipleCharacterSearchTerm);

    await wait(() => {
      const searchResultsItems = getAllByTestId('pickupLocationResultsItem');
      expect(searchResultsItems.length).toBeGreaterThan(0);
    });
  });
  it('shows results when mulitple characters are entered and then shows no results when only a single character is entered', async () => {
    const { pickupLocation, getByTestId, getAllByTestId } = renderComponent();
    const multipleCharacterSearchTerm = faker.random.alphaNumeric(2);
    changeFieldValue(pickupLocation, multipleCharacterSearchTerm);

    await wait(() => {
      const searchResultsItems = getAllByTestId('pickupLocationResultsItem');
      expect(searchResultsItems.length).toBeGreaterThan(0);
    });

    const singleCharacterSearchTerm = faker.random.alphaNumeric(1);
    changeFieldValue(pickupLocation, singleCharacterSearchTerm);

    await wait(() => {
      const pickupLocationResults = getByTestId('pickupLocationResults');
      expect(pickupLocationResults.children.length).toBe(0);
    });
  });
});
