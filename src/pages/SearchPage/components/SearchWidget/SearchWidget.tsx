import React, { useState, useEffect, FC } from 'react';
import './SearchWidget.style.css';

import { PickupLocation, SearchPageApi } from '../../SearchPage.service';
import useApi from '../../../../common/useApi';
import { PickupLocationResults } from './components/PickupLocationResults';

interface Props {
  service: SearchPageApi;
}

export const SearchWidget: FC<Props> = ({ service }: Props) => {
  const [searchValue, setSearchValue] = useState('');
  const [searchResults, setSearchResults] = useState<PickupLocation[]>([]);
  const [getPickupLocationsResponse, getPickupLocations] = useApi(
    service.getPickupLocations
  );

  useEffect(() => {
    if (searchValue.length > 1) {
      getPickupLocations(searchValue);
    } else {
      setSearchResults([]);
    }
  }, [searchValue]);

  useEffect(() => {
    if (searchValue.length > 1) {
      setSearchResults(getPickupLocationsResponse.result?.results.docs || []);
    }
  }, [searchValue, getPickupLocationsResponse.result]);

  return (
    <div>
      <div className="search-widget" data-testid="SearchWidget">
        <h2 className="search-widget__title">Let’s find your ideal car</h2>
        <div className="search-widget__controls">
          <label
            htmlFor="pickupLocation"
            className="search-widget-controls__label"
          >
            Pick-up Location
          </label>
          <div>
            <input
              type="text"
              id="pickupLocation"
              data-testid="pickupLocation"
              name="pickupLocation"
              className="search-widget-controls__input"
              placeholder="city, airport, station, region, district…"
              aria-describedby="pickupLocation-screenReaderInstructions"
              value={searchValue}
              // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
              onChange={e => setSearchValue(e.target.value)}
            ></input>
            <PickupLocationResults searchResults={searchResults} />
            <span
              data-testid="pickupLocation-screenReaderInstructions"
              id="pickupLocation-screenReaderInstructions"
              className="hidden"
            >
              Use this input to search for pickup locations.
            </span>{' '}
          </div>
        </div>
      </div>
    </div>
  );
};
