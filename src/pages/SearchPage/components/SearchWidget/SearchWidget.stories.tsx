import React from 'react';
import { storiesOf } from '@storybook/react';
import { SearchWidget } from './SearchWidget';
import centered from '@storybook/addon-centered';
import {
  PickupLocation,
  GetPickupLocationsResponse,
  SearchPageApi
} from '../../SearchPage.service';

import faker from 'faker';
import './SearchWidget.style.css';
import '../../../../styles.css';

// eslint-disable-next-line no-undef
const stories = storiesOf('components/Search Widget', module);

const style = {
  backgroundColor: '#fff',
  padding: '8rem'
};

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const getMockSearchPageService = () => {
  const generatePickupLocation = (
    index: number,
    placeType: string
  ): PickupLocation => ({
    bookingId: faker.random.alphaNumeric(10),
    country: `country ${index}`,
    name: `pickup ${index}`,
    region: `region ${index}`,
    placeType
  });

  const createPickupLocationsResponse = (): GetPickupLocationsResponse => ({
    results: {
      docs: [
        generatePickupLocation(1, 'A'),
        generatePickupLocation(2, 'Z'),
        generatePickupLocation(3, 'T'),
        generatePickupLocation(4, 'C'),
        generatePickupLocation(5, 'P'),
        generatePickupLocation(6, 'A')
      ]
    }
  });

  const service: SearchPageApi = {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    getPickupLocations: (searchTerm: string) =>
      Promise.resolve({
        status: 200,
        statusText: 'OK',
        headers: {},
        config: {},
        data: createPickupLocationsResponse()
      })
  };

  return service;
};

stories.addDecorator(centered);

stories.add('Default', () => (
  <div style={style}>
    {<SearchWidget service={getMockSearchPageService()} />}
  </div>
));
