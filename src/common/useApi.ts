/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState } from 'react';
import { AxiosPromise } from 'axios';

export interface ApiResponse<T> {
  result?: T | undefined;
  isError: boolean;
  isFetching: boolean;
  isRefetching: boolean;
}

type UseApiResponse<T> = [
  ApiResponse<T>,
  (...args: any[]) => Promise<T | undefined>
];

export default function useApi<T>(
  serviceMethod: (...args: any[]) => AxiosPromise<T>
): UseApiResponse<T> {
  const [result, setResult] = useState<T | undefined>(undefined);
  const [isFetching, setFetching] = useState(false);
  const [isRefetching, setRefetching] = useState(false);
  const [isError, setIsError] = useState(false);

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  const apiCallback = async (...args: any[]) => {
    setIsError(false);
    result ? setRefetching(true) : setFetching(true);

    try {
      const axiosResponse = await serviceMethod(...args);
      setResult(axiosResponse.data);
      setRefetching(false);
      setFetching(false);
      return axiosResponse.data;
    } catch (error) {
      setIsError(true);
      setRefetching(false);
      setFetching(false);
      return;
    }
  };

  const apiResponse = {
    result,
    isError,
    isFetching,
    isRefetching
  };
  return [apiResponse, apiCallback];
}
