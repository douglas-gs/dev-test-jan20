export const pickupLocationSearchBaseUrl: string =
  'https://cors-anywhere.herokuapp.com/https://www.rentalcars.com';

export const numResultsToReturn = 6;
