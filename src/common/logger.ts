import { AxiosError, AxiosResponse } from 'axios';

export interface LogAxiosEvent {
  response?: AxiosResponse;
  error?: AxiosError;
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const logAxiosEventToConsole = ({ response, error }: LogAxiosEvent) => {
  console.groupCollapsed('Axios Event');
  if (error) {
    console.log(
      `%cApi Response Failure: ${error.config.url}`,
      'color: #F64744'
    );
    console.log(`error message: ${error.message}`);
    console.log(`config: ${error.config}`);
    console.log(`request: ${error.request}`);
  } else if (response) {
    if (response.status >= 200 && response.status < 300) {
      console.log(
        `%cApi Response Success: ${response.config.url}`,
        'color: #66B34E'
      );
    } else {
      console.log(
        `%cApi Response Failure: ${response.config.url}`,
        'color: #F64744'
      );
      console.log(`status: ${response.status}`);
    }
    console.log(`Axios Response Object: ${response}`, response);
  } else {
    return;
  }
  console.groupEnd();
};
