import axios from 'axios';
import { logAxiosEventToConsole } from './logger';
import { pickupLocationSearchBaseUrl } from './config';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const getInstance = () => {
  const baseURL = pickupLocationSearchBaseUrl;

  const instance = axios.create({
    baseURL
  });

  instance.interceptors.response.use(
    response => {
      logAxiosEventToConsole({ response });
      return Promise.resolve(response);
    },
    error => {
      logAxiosEventToConsole({ error });
      return Promise.reject(error.response || error);
    }
  );
  return instance;
};
