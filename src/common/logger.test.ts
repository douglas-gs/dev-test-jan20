/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { logAxiosEventToConsole, LogAxiosEvent } from './logger';
import { AxiosError, AxiosResponse } from 'axios';

// eslint-disable-next-line @typescript-eslint/no-empty-function
console.groupCollapsed = () => {};

const axiosError: AxiosError = {
  isAxiosError: true,
  name: 'name',
  config: { url: 'url', baseURL: 'http://base-url' },
  request: 'request',
  message: 'error'
};

const axios200Response: AxiosResponse = {
  data: { some: 'value' },
  config: { url: 'url', baseURL: 'http://base-url' },
  status: 200,
  statusText: 'OK',
  headers: {}
};

const axios400Response: AxiosResponse = {
  data: {},
  config: { url: 'url', baseURL: 'http://base-url' },
  status: 400,
  statusText: 'Bad request',
  headers: {}
};

beforeAll(() => {
  // Create a spy on console (console.log in this case) and provide some mocked implementation
  // In mocking global objects it's usually better than simple `jest.fn()`
  // because you can `unmock` it in clean way doing `mockRestore`
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  jest.spyOn(console, 'log').mockImplementation(() => {});
});

afterEach(() => {
  // Clear mock (all calls etc) after each test.
  // It's needed when you're using console somewhere in the tests so you have clean mock each time
  console.log.mockClear();
});

afterAll(() => {
  console.log.mockRestore();
});

describe('Logger', () => {
  describe('logAxiosEventToConsole', () => {
    test('will not log if neither error nor response are given', async () => {
      const event: LogAxiosEvent = {};
      logAxiosEventToConsole(event);
      expect(console.log).not.toHaveBeenCalled();
    });
    test('will log error message if error provided', async () => {
      const event: LogAxiosEvent = { error: axiosError };
      logAxiosEventToConsole(event);
      expect(console.log).toHaveBeenNthCalledWith(
        1,
        `%cApi Response Failure: ${axiosError.config.url}`,
        `color: #F64744`
      );
      expect(console.log).toHaveBeenNthCalledWith(
        2,
        `error message: ${axiosError.message}`
      );
      expect(console.log).toHaveBeenNthCalledWith(
        3,
        `config: ${axiosError.config}`
      );
      expect(console.log).toHaveBeenNthCalledWith(
        4,
        `request: ${axiosError.request}`
      );
    });
    test('will log error message if response is not 200', async () => {
      const event: LogAxiosEvent = { response: axios400Response };
      logAxiosEventToConsole(event);
      expect(console.log).toHaveBeenNthCalledWith(
        1,
        `%cApi Response Failure: ${axios400Response.config.url}`,
        `color: #F64744`
      );
      expect(console.log).toHaveBeenNthCalledWith(
        2,
        `status: ${axios400Response.status}`
      );
      expect(console.log).toHaveBeenNthCalledWith(
        3,
        `Axios Response Object: ${axios400Response.data}`,
        axios400Response
      );
    });
    test('Will log success message if response is 200', async () => {
      const event: LogAxiosEvent = { response: axios200Response };
      logAxiosEventToConsole(event);
      expect(console.log).toHaveBeenNthCalledWith(
        1,
        `%cApi Response Success: ${axios200Response.config.url}`,
        `color: #66B34E`
      );
      expect(console.log).toHaveBeenNthCalledWith(
        2,
        `Axios Response Object: ${axios200Response.data}`,
        axios200Response
      );
      expect(console.log).not.toHaveBeenCalledWith(
        `error message: ${axiosError.message}`
      );
    });
  });
});
