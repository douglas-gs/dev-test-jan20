import { renderHook } from '@testing-library/react-hooks';
import useApi, { ApiResponse } from './useApi';
import MockAdapter from 'axios-mock-adapter';
import Axios, { AxiosPromise, AxiosInstance } from 'axios';

interface MockApi {
  mockGetServiceMethod: () => AxiosPromise<string>;
}
const MockService = (axios: AxiosInstance): MockApi => {
  return {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    mockGetServiceMethod: () => axios.get('/test')
  };
};

const axiosMock = new MockAdapter(Axios);
const mockService = MockService(Axios);

describe('useApi', () => {
  beforeEach(() => {
    axiosMock.reset();
  });

  test('triggers only when callback is called', async () => {
    axiosMock.onGet('/test').reply(200, 'data');

    const { result, waitForNextUpdate } = renderHook(() =>
      useApi(mockService.mockGetServiceMethod)
    );
    // eslint-disable-next-line prefer-const
    let [apiResponse, apiCallback] = result.current;
    const emptyApiResponse: ApiResponse<string> = {
      result: undefined,
      isFetching: false,
      isRefetching: false,
      isError: false
    };

    expect(apiResponse).toStrictEqual(emptyApiResponse);

    apiCallback();
    await waitForNextUpdate();
    [apiResponse] = result.current;

    expect(apiResponse.result).toEqual('data');
  });

  test('sets isError to true if network error occurs', async () => {
    axiosMock.onGet('/test').networkError();

    const { result, waitForNextUpdate } = renderHook(() =>
      useApi(mockService.mockGetServiceMethod)
    );
    // eslint-disable-next-line prefer-const
    let [apiResponse, apiCallback] = result.current;
    apiCallback();

    expect(apiResponse.isError).toBe(false);

    await waitForNextUpdate();
    [apiResponse] = result.current;

    expect(apiResponse.isError).toBe(true);
  });

  test('sets error if backend error occurs', async () => {
    axiosMock.onGet('/test').reply(500);

    const { result, waitForNextUpdate } = renderHook(() =>
      useApi(mockService.mockGetServiceMethod)
    );
    // eslint-disable-next-line prefer-const
    let [apiResponse, apiCallback] = result.current;
    apiCallback();

    expect(apiResponse.isError).toBe(false);

    await waitForNextUpdate();
    [apiResponse] = result.current;

    expect(apiResponse.isError).toBe(true);
  });

  test('re-sets error when new fetch is initiated', async () => {
    // 1st call - yields an error
    axiosMock.onGet('/test').reply(500);
    const { result, waitForNextUpdate } = renderHook(() =>
      useApi(mockService.mockGetServiceMethod)
    );
    // eslint-disable-next-line prefer-const
    let [apiResponse, apiCallback] = result.current;
    apiCallback();

    expect(apiResponse.isError).toBe(false);

    await waitForNextUpdate();
    [apiResponse] = result.current;

    expect(apiResponse.isError).toBe(true);

    // 2nd call - does not yield an error, previous error should not persist
    axiosMock.onGet('/test').reply(200, 'data');
    apiCallback();
    [apiResponse] = result.current;

    expect(apiResponse.isError).toBe(false);

    await waitForNextUpdate();
    [apiResponse] = result.current;

    expect(apiResponse.isError).toBe(false);
  });

  test('sets result if successful', async () => {
    axiosMock.onGet('/test').reply(200, 'data');

    const { result, waitForNextUpdate } = renderHook(() =>
      useApi(mockService.mockGetServiceMethod)
    );
    const [, apiCallback] = result.current;
    apiCallback();

    await waitForNextUpdate();
    const [apiResponse] = result.current;

    expect(apiResponse.result).toEqual('data');
  });

  test('sets isFetching to true when fetching and re-sets once finished', async () => {
    axiosMock.onGet('/test').reply(200, 'data');

    const { result, waitForNextUpdate } = renderHook(() =>
      useApi(mockService.mockGetServiceMethod)
    );
    const [, apiCallback] = result.current;
    apiCallback();
    let [apiResponse] = result.current;

    expect(apiResponse.isFetching).toEqual(true);

    await waitForNextUpdate();
    [apiResponse] = result.current;

    expect(apiResponse.isFetching).toEqual(false);
  });

  test('sets isFetching to true when fetching and upon successful fetch, sets it to false when re-fetching', async () => {
    axiosMock.onGet('/test').reply(200, 'data');

    // 1st call - isFetching should be true, isRefeteching false
    const { result, waitForNextUpdate } = renderHook(() =>
      useApi(mockService.mockGetServiceMethod)
    );
    let [, apiCallback] = result.current;
    apiCallback();
    let [apiResponse] = result.current;

    expect(apiResponse.isFetching).toEqual(true);
    expect(apiResponse.isRefetching).toEqual(false);

    await waitForNextUpdate();

    // 2nd call - isFetching should be false, isRefeteching true
    [apiResponse, apiCallback] = result.current;
    apiCallback();
    [apiResponse] = result.current;

    expect(apiResponse.isFetching).toEqual(false);
    expect(apiResponse.isRefetching).toEqual(true);
  });
});
