[![js-standard-style](https://img.shields.io/badge/code%20style-airbnb-brightgreen.svg?style=flat)](https://github.com/airbnb/javascript)

# Developer Test January 2020

## 👋 Intro

This repo is for a developer test in January 2020

## ☑️ Specifics

- **JS Version**: ES6
- **Scaffolding**: [Create React App](https://github.com/facebook/create-react-app)
- **Unit/Integration Testing**: [Jest](https://jestjs.io/) and [RTL](https://github.com/testing-library/react-testing-library)
- **UI Library**: [React](https://reactjs.org/)
- **Linting**: [ESlint](https://github.com/eslint/eslint)
- **Formatting**: [Prettier](https://github.com/prettier/prettier)

---

## 🚀 Setup

#### 1. Clone and Install

```bash
# Clone the repo
git clone https://douglas-gs@bitbucket.org/douglas-gs/dev-test-jan20.git

# Traverse to the source directory
cd dev-test-jan20/src/

# Install dependencies
npm i
```

For windows machines, you may need to install ESLint globally for husky pre-commit hooks to work as expected with the following command:
```bash
npm install -g eslint
```

**Plese note, the current version of Create React App requires a tweak to work with Edge and IE.**

**[Information on the CRA IE / Edge fix can this can be found here](https://github.com/facebook/create-react-app/issues/6924)**

#### 2. Run the App

```bash
npm start
```

#### 3. Test the App

This command assumes you want to run a one off test. Please note, some "use act(() =>{})" warnings have been ignored as they are a real pain when creating bespoke hooks.

```bash
npm test -- --watchAll=false
```

If you want to run a test watcher, the following command is what you're after.
```bash
npm test
```

#### 4. Run the Storybok

```bash
npm run storybook
```

## Approach

My approach to this test is to use [Create React App (CRA)](https://github.com/facebook/create-react-app) to bootstrap the project and then to add the most important development components to showcase how I develop to production strength.

### Mobile considerations
- I would look to use a Progress Web App wth service workers to improve the user experience.
- Us media queries to better tailor the site to different screen sizes.
- Include a manifesto.json with various icon sizes for different devices 

### Assumptions / Best practices

As there is no Business Analyst with whom to kick off the work, I will use the live site to answer questions I have. My standard approach is to discuss unit tests with the Quality Assurance (QA) / Test team. I'm happy to write end to end tests or discuss stories with the QA team before they write the tests.

As there is only 1 developer on the project, no code reviews will be undertaken. My preference is to create a feature or bug-fix branch per feature / bug ticket, have this code reviewed and then merged into the master branch.

All inital config setup will be done against the master branch with subsequent updates through branches. Without fellow developers, merge conflicts will not occur, these are a key skill for team development.

### Core development technologies

- Storybook - installed and configured for CRA
- Babel - using CRA defaults
- Linting - standard CRA linting rules being used, Husky used for pre-commit hooks to fix and abort if necessary 
- Unit testing - using React Testuing Library that comes with CRA

### What's missing?

- There is no build pipeline, it can only be run locally.
- Cookie policy 
- Test id attribute removal
- No robots.txt - not being deployed so not necessary
- Ticket numbers on commit messages
- End to end tests - my current preference is to use test cafe for it's IE coverage
- Peer reviews

## CRA Scripts

I have left the CRA documentation as is.

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify