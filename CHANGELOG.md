## 1.0.0 (2020-01-14)

- Initial app from create-react-app
- Added storybook

## 1.0.1 (2020-01-14)

- Configure linting for husky
- Add Husky
- Configure husky for pre-commit linting

## 1.0.2 (2020-01-14)

- Update ReadMe
- Add editorconfig
- Fix linting issue in App (used to check husky and ESLint working as expected)

## 1.1.0 (2020-01-14)

- Remove service worker
- Bump version in package.json and add update .lock
- Add more info to ReadMe
- Fix prettier issues 
- Change app to empty search page

## 1.2.0 (2020-01-14) Test 1

- Add search widget styled as per homepage (AC1)
- Add placeholder to input of search widget styled as per homepage (AC2)
- Add focus for input box (AC3) - copied homepage for styling of focus
- Add screen reader inbstructions for location pickup (AC4)

## 1.2.1 (2020-01-14) Test 1 with multi-browser support

- Add support for Edge
- Add support for IE 10 & 11

N.B.

Plese note, the current version of Create React App requires a tweak to work with Edge and IE.
[Information on the CRA IE / Edge fix can this can be found here](https://github.com/facebook/create-react-app/issues/6924)

# 1.3.0 (2020-01-16) Test 2 (AC1 - AC5)

- Add pickup location search results component
- Extract search results component from search widget, update story, complete tests
- Update main app entry point to use search page service
- Add testing helper to change field value
- Add search page service with test, mock and config
- Add useApi hook, required for search page service
- Extra modules required for more advanced testing
- Add data request infrastructure
